import 'package:fake_store/page/home/home_page.dart';
import 'package:fake_store/page/splash/splash_page.dart';
import 'package:fake_store/routes/locate.dart';
import 'package:fake_store/routes/navigator_service.dart';
import 'package:fake_store/routes/router.dart';
import 'package:flutter/material.dart';

void main() {
  setupLocate();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
      
        primarySwatch: Colors.blue,
      ),
      home: SplashPage(),
      navigatorKey: locate<NavigatorService>().navigatorKey,
      onGenerateRoute: (settings) => RouteGenerator.generateRoute(settings), 
    );
  }
}


