import 'package:fake_store/routes/navigator_service.dart';
import 'package:get_it/get_it.dart';

GetIt locate = GetIt.asNewInstance();

void setupLocate(){
  locate.registerLazySingleton(() => NavigatorService());
}