import 'package:flutter/material.dart';

class NavigatorService{
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName){
    return navigatorKey.currentState!.pushNamed(routeName);
  }

  Future<dynamic> navigateToWithArgmnt(String routeName, Object args){
    return navigatorKey.currentState!.pushNamed(routeName, arguments: args);
  }

  void goBack({value}){
    return navigatorKey.currentState!.pop(value);
  }
}