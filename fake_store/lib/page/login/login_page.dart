import 'package:fake_store/routes/locate.dart';
import 'package:fake_store/routes/navigator_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(Icons.shopping_bag_outlined),
              RichText(
                text: TextSpan(
                    style: DefaultTextStyle.of(context).style,
                    children: const <TextSpan>[
                      TextSpan(
                          text: 'Fake',
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.lightGreen)),
                      TextSpan(
                          text: 'Store',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold))
                    ]),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Text('Username'),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 40,
            padding: EdgeInsets.all(8),
            width: double.infinity,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10)),
            child: TextFormField(
              decoration:
                  const InputDecoration.collapsed(hintText: 'input username'),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          const Text('Password'),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 40,
            padding: const EdgeInsets.all(8),
            width: double.infinity,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                borderRadius: BorderRadius.circular(10)),
            child: TextFormField(
              obscureText: true,
              decoration: InputDecoration.collapsed(hintText: 'input password'),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width / 1,
            child: ElevatedButton(
              onPressed: () {},
              child: Text(
                'Submit',
                style: TextStyle(color: Colors.black),
              ),
              style: ElevatedButton.styleFrom(primary: Colors.lightGreen),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children:  [
              const Text("Don't have account?"),
               Padding(
                padding: const EdgeInsets.only(right: 80),
                child: InkWell(
                  onTap: (){
                    locate<NavigatorService>().navigateTo('/register');
                  },
                  child: const Text(
                    'Sign Up',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: (){
                  
                },
                child: const Text(
                  'Forgot password?',
                  style: TextStyle(color: Colors.blue),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
