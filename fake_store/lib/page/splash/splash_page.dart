import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:fake_store/page/login/login_page.dart';
import 'package:fake_store/routes/locate.dart';
import 'package:fake_store/routes/navigator_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
  }

  @override 
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      backgroundColor: Colors.lightGreen,
      
      splash: Column(
        children: const [
          Text(
            'FK',
            style: TextStyle(
                fontSize: 50, color: Colors.white, fontWeight: FontWeight.bold),
          ),

          Text('Fake Store', style: TextStyle(color: Colors.white),)
        ],
      ),
      nextScreen: const LoginPage(),
      duration: 4000,
      splashTransition: SplashTransition.slideTransition,
    );
  }
}
